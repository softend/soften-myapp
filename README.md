heroku url: https://spec-projectcn340.herokuapp.com/

Iteration 1
วิธีการ Test:
    #bundle install --withput production
    #rails db:migrate RAILS_ENV=test
    -Model: #rails test test/models/user_test.rb
        -คำสั่งทดสอบการสมัครสมาชิก #rails test test/integration/users_signup_test.rb
        -คำสั่งทดสอบการเข้าสู่ระบบและการออกจากระบบ #rails test test/integration/users_login_test.rb
        

Iteration 2 
Deadline 1
วิธีการ Test:
    คำสั่งในส่วนของการทดสอบ Model ของการโพสต์ทั้งหมด ==> #rails test test/models/micropost_test.rb
    จะประกอบไปด้วยการทดสอบทั้งหมดดังนี้
        -ทดสอบระบบว่าควรมีการใส่ข้อมูลในการโพสต์ :Feature Create, Update
        -ทดสอบระบบว่าuser id ที่ใช้ระบบอยู่ควรเป็นคนปัจจุบัน :Feature Create, Update
        -ทดสอบระบบว่าเมื่อมีการกรอกข้อมูลแล้วระบบควรมีเนื้อหาอยู่ :Feature Create, Update
        -ทดสอบระบบว่าข้อมูลที่กรอกควรมีเนื้อหาไม่เกิน 255 อักขระ :Feature Create, Update
        -ทดสอบระบบว่าควรมีการจัดลำดับโพสที่โพสต์ครั้งล่าสุดไว้ในอันดับแรก :Feature View, Search
        -ทดสอบระบบว่าควรค้นหาสเปคเจอเมื่อมีการกรอกราคารวมอยู่ในช่วงที่ถูกต้องและไม่เจอเมื่อมีการกรอกราคาที่ไม่อยู่ในช่วง :Feature Search
        -ทดสอบระบบว่าควรค้นหาสเปคทั้งหมดได้จำนวนผลลัพธ์ที่ออกมาเท่ากับจำนวนสเปคทั้งหมดที่มีอยู่ในฐานข้อมูล :Feature Search
        
Deadline 2
วิธีการ Test:
    คำสั่งในส่วนของการทดสอบ Model ของการโพสต์ทั้งหมด(สามารถรันผ่านได้ทั้งหมด) ==> #rails test test/models/user_test.rb
    คำสั่งในส่วนของการทดสอบ Controller ของการทำงานในการโพสต์ ==> #rails test test/controller/microposts_controller_test.rb
    ยังไม่สามารถทดสอบผ่านได้ในหัวข้อดังต่อไปนี้
    - test "should get new" 
    - test "should create micropost"
    - test "should get edit" 
    - test "should update micropost"
    - test "should destroy micropost" 
    
    
Iteration 3
Deadline 1, 2
วิธีการ Test:
    จะเป็นคำสั่งในส่วนของการทดสอบ feature admin ทั้งหมด ==> #rails test test/models/user_test.rb
    ซึ่งจะประกอบไปด้วยหัวข้อการทดสอบดังนี้
    - test "Others user should not allow using administrator privileges to be edited other post via the web"
    - test "Admin should allow to be edited all post via the web"
    - test "Admin should allow delete all post via the web" 