class CreateMicroposts < ActiveRecord::Migration[5.1]
  def change
    create_table :microposts do |t|
      t.references :user, foreign_key: true
      t.string :cpu,null: false
      t.string :detail_cpu,null: false
      t.string :mainboard,null: false
      t.string :detail_mainboard,null: false
      t.string :ram,null: false
      t.string :detail_ram,null: false
      t.string :storage,null: false
      t.string :detail_storage,null: false
      t.string :vga,null: false
      t.string :detail_vga,null: false
      t.string :power,null: false
      t.string :detail_power,null: false
      t.string :case,null: false
      t.string :detail_case,null: false
      t.integer :price,null: false

      t.timestamps
    end
    add_index :microposts, [:user_id, :created_at]
  end
end
