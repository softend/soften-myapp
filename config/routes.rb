Rails.application.routes.draw do
  
  

  resources :microposts
  root 'static_pages#home'
  get '/about', to:'static_pages#about'
  get '/signup', to: 'users#new'
  post '/signup',to: 'users#create'
  get '/signin', to: 'sessions#new'
  post '/signin', to: 'sessions#create'
  delete '/signout', to: 'sessions#destroy'
  resources :users
  get '/posts',to: 'microposts#new'
  get '/search_all',to: 'microposts#index'
  
end
