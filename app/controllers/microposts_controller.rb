class MicropostsController < ApplicationController
  before_action :set_micropost, only: [:show, :edit, :update, :destroy]
  before_action :logged_in_user, only: [:edit,:create, :destroy]
  before_action :admin_or_user, only: [:edit,:destroy]
  # GET /microposts
  # GET /microposts.json
  def index
    @microposts = Micropost.search(params[:Starting_price],params[:Highest_price])
  end

  # GET /microposts/1
  # GET /microposts/1.json
  def show
     @microposts = Micropost.all
  end
  
  # GET /microposts/new
  def new
   # @micropost = current_user.microposts.build if logged_in?
    @micropost = Micropost.new
  end

  # GET /microposts/1/edit
  def edit
  end

  # POST /microposts
  # POST /microposts.json
  def create
    @micropost = Micropost.new(micropost_params)

    respond_to do |format|
      if @micropost.save
        format.html { redirect_to @micropost, notice: 'The post was successfully created.' }
        format.json { render :show, status: :created, location: @micropost }
      else
        format.html { render :new }
        format.json { render json: @micropost.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /microposts/1
  # PATCH/PUT /microposts/1.json
  def update
    respond_to do |format|
      if @micropost.update(micropost_params)
        format.html { redirect_to @micropost, notice: 'The post was successfully updated.' }
        format.json { render :show, status: :ok, location: @micropost }
      else
        format.html { render :edit }
        format.json { render json: @micropost.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /microposts/1
  # DELETE /microposts/1.json
  def destroy
    @micropost.destroy
    respond_to do |format|
      format.html { redirect_to microposts_path, notice: 'The post was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_micropost
      @micropost = Micropost.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def micropost_params
      params.require(:micropost).permit(:user_id, :cpu, :detail_cpu, :mainboard, :detail_mainboard, :ram, :detail_ram, :storage, :detail_storage, :vga, :detail_vga, :power, :detail_power, :case, :detail_case, :price)
    #params.require(:micropost).permit(:user_id, :cpu, :detail_cpu.:price_cpu, :mainboard, :detail_mainboard.:price_mainboard, :ram, :detail_ram.:price_ram, :storage, :detail_storage.:price_storage, :vga, :detail_vga.:price_vga, :power, :detail_power.:price_power, :case, :detail_case.:price_case,:price_cpu+:price_mainboard+:price_ram+:price_storage+:price_vga+:price_power+:price_case)
    end
    
    #def correct_user
    #  if !current_user.admin? 
    #    redirect_to root_url
    #  end
    #end
    
    def admin_or_user
      if logged_in? || current_user.admin?
      else
        redirect_to(root_url)
      end
    end
end
