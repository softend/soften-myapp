
//= require jquery
//= require bootstrap
//= require rails-ujs
//= require turbolinks
//= require_tree .
//= require bootstrap/bootstrap-tooltip
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})