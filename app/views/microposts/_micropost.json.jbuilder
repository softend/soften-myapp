json.extract! micropost, :id, :user_id, :cpu, :detail_cpu, :mainboard, :detail_mainboard, :ram, :detail_ram, :storage, :detail_storage, :vga, :detail_vga, :power, :detail_power, :case, :detail_case, :price, :created_at, :updated_at
json.url micropost_url(micropost, format: :json)
