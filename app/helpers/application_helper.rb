module ApplicationHelper
    def full_tile(page_title = '')
        base_title = "Spec-PC Application"
        if page_title.empty?
            base_title
        else
            page_title + "||" + base_title
        end
    end
end