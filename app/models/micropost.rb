class Micropost < ApplicationRecord
  belongs_to :user
  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :cpu, presence: true, length: { maximum: 255 }
  validates :detail_cpu, presence: true, length: { maximum: 255 }
  validates :mainboard, presence: true, length: { maximum: 255 }
  validates :detail_mainboard, presence: true, length: { maximum: 255 }
  validates :ram, presence: true, length: { maximum: 255 }
  validates :detail_ram, presence: true, length: { maximum: 255 }
  validates :storage, presence: true, length: { maximum: 255 }
  validates :detail_storage, presence: true, length: { maximum: 255 }
  validates :vga, presence: true, length: { maximum: 255 }
  validates :detail_vga, presence: true, length: { maximum: 255 }
  validates :power, presence: true, length: { maximum: 255 }
  validates :detail_power, presence: true, length: { maximum: 255 }
  validates :case, presence: true, length: { maximum: 255 }
  validates :detail_case, presence: true, length: { maximum: 255 }
  validates :price, presence: true, length: { maximum: 7 }
  
  def self.search(start,high)
    if start
      where("price >= ? and price <= ?",start,high)
    else
      all
    end
  end
end
