require 'test_helper'

class UserTest < ActionDispatch::IntegrationTest
  def setup
    @user = User.new(name:"tar",email:"tar@to.co.th",password: "p@ssw0rd", 
    password_confirmation: "p@ssw0rd")
    @user.save
    @other_user = User.new(name:"james", email:"jamemers@gaming.com",password: "123456789",
    password_confirmation: "123456789")
    @other_user.save
    @admin = User.new(name:"admin", email:"admin@tu.ac.com",password: "123456",
    password_confirmation: "123456" ,admin: true)
    @admin.save
    @micropost = microposts(:most_recent)
  end
  
  test "should be valid" do
    assert @user.valid?
  end
  
  test "name should be present" do
    @user.name = " "
    assert_not @user.valid?
  end
  
  test "email should be present" do
    @user.email = "     "
    assert_not @user.valid?
  end
  
  test "name should not be too long" do
    @user.name = "a" * 51
    assert_not @user.valid?
  end
  
  test "email should not be too long" do
    @user.email = "a" * 244 + "@tu.com"
    assert_not @user.valid?
  end
  
  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end
    
  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end
  
  test "email addresses should be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end
  
  test "email addresses should be saved as lower-case" do
    mixed_case_email = "Foo@ExAMPle.CoM"
    @user.email = mixed_case_email
    @user.save
    assert_equal mixed_case_email.downcase, @user.reload.email
  end
  
  test "password should be present (nonblank)" do
    @user.password = @user.password_confirmation = " " * 6
    assert_not @user.valid?
  end

  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end
  
#>>>>>>> ite2
  test "should get index" do
    get microposts_path
    assert_response :success
  end
  
  test "should show micropost" do
    get microposts_path(@micropost)
    assert_response :success
  end

  test "microposts should be destroyed" do  
    #@micropost = @user.microposts.create!(cpu:"INTEL Corei7-8700K", detail_cpu:"JIB, 35000",mainboard: "asus z270",detail_mainboard: "jib 10000",ram:"Kington DDR100 1TB",detail_ram:"JIB, 2000",storage:"WD 4 TB",detail_storage:"JIB, 1000",vga:"ASUS Geforce 2080TI",detail_vga:"JIB, 3000",power:"CORSAIR 20000 watt",detail_power:"JIB, 2000",case:"Gigabyte modelz",detail_case:"JIB, 1000",price: 100000,created_at: 5.minutes.ago,updated_at: 5.minutes.ago)
    @micropost = @user.microposts.new(cpu:"INTEL Corei7-8700K", detail_cpu:"JIB, 35000",mainboard: "asus z270",detail_mainboard: "jib 10000",ram:"Kington DDR100 1TB",detail_ram:"JIB, 2000",storage:"WD 4 TB",detail_storage:"JIB, 1000",vga:"ASUS Geforce 2080TI",detail_vga:"JIB, 3000",power:"CORSAIR 20000 watt",detail_power:"JIB, 2000",case:"Gigabyte modelz",detail_case:"JIB, 1000",price: 100000,created_at: 5.minutes.ago,updated_at: 5.minutes.ago)
    @micropost.save
    assert_difference 'Micropost.count', -1 do
      @micropost.destroy
    end
  #  @user.save
  #  @user.microposts.create!(cpu:"INTEL Corei7-8700K", detail_cpu:"JIB, 35000",ram:"Kington DDR100 1TB",detail_ram:"JIB, 2000",storage:"WD 4 TB",detail_storage:"JIB, 1000",vga:"ASUS Geforce 2080TI",detail_vga:"JIB, 3000",power:"CORSAIR 20000 watt",detail_power:"JIB, 2000",case:"Gigabyte modelz",detail_case:"JIB, 1000",price: 100000,created_at: 5.minutes.ago,updated_at: 5.minutes.ago)
  #  assert_difference 'Micropost.count', -1 do
  #    @user.destroy
  #  end
  end
#=======
   #test "associated microposts should be destroyed" do
    #@user.microposts.new(user_id: @user.id ,cpu:"INTEL Corei7-8700K", detail_cpu:"JIB, 35000",ram:"Kington DDR100 1TB",detail_ram:"JIB, 2000",storage:"WD 4 TB",detail_storage:"JIB, 1000",vga:"ASUS Geforce 2080TI",detail_vga:"JIB, 3000",power:"CORSAIR 20000 watt",detail_power:"JIB, 2000",case:"Gigabyte modelz",detail_case:"JIB, 1000",price: 100000,created_at: Time.zone.now,updated_at: Time.zone.now)
    #assert_difference 'Micropost.count', -1 do
      #@user.destroy
    #end
  #end
  

  test "Users should only edit or update their posts" do
    get signin_path
    post signin_path,params:{session:{email: @user.email,password:@user.password}} #log_in_as(@user)
    assert is_logged_in?
    patch micropost_path(@micropost), params: { micropost: { user_id: @micropost.user_id, cpu: @micropost.cpu, detail_cpu: @micropost.detail_cpu, ram: @micropost.ram, detail_ram: @micropost.detail_ram, storage: @micropost.storage,  detail_storage: @micropost.detail_storage, vga: @micropost.vga, detail_vga: @micropost.detail_vga, power: @micropost.power, detail_power: @micropost.detail_power, case: @micropost.case, detail_case: @micropost.detail_case,  price: @micropost.price, created_at: @micropost.created_at,updated_at: @micropost.updated_at } }
    assert_redirected_to micropost_path(@micropost)
  end
  
  test "Users should only delete their posts" do
    get signin_path
    post signin_path,params:{session:{email: @user.email,password:@user.password}} #log_in_as(@user)
    assert is_logged_in?
    @micropost = @user.microposts.new(cpu:"INTEL Corei7-8700K", detail_cpu:"JIB, 35000",mainboard: "asus z270",detail_mainboard: "jib 10000",ram:"Kington DDR100 1TB",detail_ram:"JIB, 2000",storage:"WD 4 TB",detail_storage:"JIB, 1000",vga:"ASUS Geforce 2080TI",detail_vga:"JIB, 3000",power:"CORSAIR 20000 watt",detail_power:"JIB, 2000",case:"Gigabyte modelz",detail_case:"JIB, 1000",price: 100000,created_at: 5.minutes.ago,updated_at: 5.minutes.ago)
    @micropost.save
    assert_difference 'Micropost.count', -1 do
      @micropost.destroy
    end
  end
  
#>>>>>>> ite3
  
  test "Others user should not allow using administrator privileges to be edited other post via the web" do 
     get signin_path
     post signin_path,params:{session:{email: @other_user.email,password:@other_user.password}} #log_in_as(@other_user)
     assert is_logged_in?
     assert_not @other_user.admin?
     if @other_user.admin?
        patch micropost_path(@micropost), params: { micropost: { user_id: @micropost.user_id, cpu: @micropost.cpu, detail_cpu: @micropost.detail_cpu, ram: @micropost.ram, detail_ram: @micropost.detail_ram, storage: @micropost.storage,  detail_storage: @micropost.detail_storage, vga: @micropost.vga, detail_vga: @micropost.detail_vga, power: @micropost.power, detail_power: @micropost.detail_power, case: @micropost.case, detail_case: @micropost.detail_case,  price: @micropost.price, created_at: @micropost.created_at,updated_at: @micropost.updated_at } }
        assert_redirected_to micropost_path(@micropost)
     end
  end
  
  test "Admin should allow to be editted all post via the web" do 
     get signin_path
     post signin_path,params:{session:{email: @admin.email,password:@admin.password}} #log_in_as(@admin)
     assert is_logged_in?
     assert @admin.admin?
     if @admin.admin?
        patch micropost_path(@micropost), params: { micropost: { user_id: @micropost.user_id, cpu: @micropost.cpu, detail_cpu: @micropost.detail_cpu, ram: @micropost.ram, detail_ram: @micropost.detail_ram, storage: @micropost.storage,  detail_storage: @micropost.detail_storage, vga: @micropost.vga, detail_vga: @micropost.detail_vga, power: @micropost.power, detail_power: @micropost.detail_power, case: @micropost.case, detail_case: @micropost.detail_case,  price: @micropost.price, created_at: @micropost.created_at,updated_at: @micropost.updated_at } }
        assert_redirected_to micropost_path(@micropost)
     end
  end
  
  test "Admin should allow delete all post via the web" do 
    get signin_path
    post signin_path,params:{session:{email: @admin.email,password:@admin.password}} #log_in_as(@admin)
    assert is_logged_in?
    assert @admin.admin?
    @micropost = @other_user.microposts.new(cpu:"INTEL Corei7-8700K", detail_cpu:"JIB, 35000",mainboard: "asus z270",detail_mainboard: "jib 10000",ram:"Kington DDR100 1TB",detail_ram:"JIB, 2000",storage:"WD 4 TB",detail_storage:"JIB, 1000",vga:"ASUS Geforce 2080TI",detail_vga:"JIB, 3000",power:"CORSAIR 20000 watt",detail_power:"JIB, 2000",case:"Gigabyte modelz",detail_case:"JIB, 1000",price: 100000,created_at: 5.minutes.ago,updated_at: 5.minutes.ago)
    @micropost.save
    assert_difference 'Micropost.count', -1 do
      @micropost.destroy
    end
  end

end
