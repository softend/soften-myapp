require 'test_helper'

class MicropostTest < ActiveSupport::TestCase
 
  def setup
    @user = users(:natchawi)
    #@user = User.new(name:"tar",email:"tar@to.co.th",password: "p@ssw0rd", 
   # password_confirmation: "p@ssw0rd")
    @user.save
    @micropost = Micropost.new(user_id: @user.id, cpu:"INTEL Corei7-8700K", detail_cpu:"JIB, 35000", mainboard: "asus z270",detail_mainboard: "jib 10000",ram:"Kington DDR100 1TB",detail_ram:"JIB, 2000",storage:"WD 4 TB",detail_storage:"JIB, 1000",vga:"ASUS Geforce 2080TI",detail_vga:"JIB, 3000",power:"CORSAIR 20000 watt",detail_power:"JIB, 2000",case:"Gigabyte modelz",detail_case:"JIB, 1000",price: 100000)
  end

  #### CREATE ####
  
  test "should be valid" do
    assert @micropost.valid?
  end
  
  test "user id should be present" do
    @micropost.user_id = nil
    assert_not @micropost.valid?
  end
  
  test "cpu should be present" do
    @micropost.cpu = nil
    assert_not @micropost.valid?
  end
  
  test "detail_cpu should be present" do
    @micropost.detail_cpu = nil
    assert_not @micropost.valid?
  end
  
  test "mainboard should be present" do
    @micropost.mainboard = nil
    assert_not @micropost.valid?
  end
  
  test "detail_mainboard should be present" do
    @micropost.detail_mainboard = nil
    assert_not @micropost.valid?
  end
  
  test "ram should be present" do
    @micropost.ram = nil
    assert_not @micropost.valid?
  end
  
  test "detail_ram should be present" do
    @micropost.detail_ram = nil
    assert_not @micropost.valid?
  end
  
  test "storage should be present" do
    @micropost.storage = nil
    assert_not @micropost.valid?
  end
  
  test "detail_storage should be present" do
    @micropost.detail_storage = nil
    assert_not @micropost.valid?
  end
  
  test "vga should be present" do
    @micropost.vga = nil
    assert_not @micropost.valid?
  end
  
  test "detail_vga should be present" do
    @micropost.detail_vga = nil
    assert_not @micropost.valid?
  end
  
  test "power should be present" do
    @micropost.power = nil
    assert_not @micropost.valid?
  end
  
  test "detail_power should be present" do
    @micropost.detail_power = nil
    assert_not @micropost.valid?
  end
  
  test "case should be present" do
    @micropost.case = nil
    assert_not @micropost.valid?
  end
  
  test "detail_case should be present" do
    @micropost.detail_case = nil
    assert_not @micropost.valid?
  end
  
  test "price should be present" do
    @micropost.price = nil
    assert_not @micropost.valid?
  end

=begin
  test "content should be at most 255 characters" do
    @micropost.cpu = "a" * 256
    @micropost.detail_cpu = "a" * 256
    @micropost.mainboard = "a" * 256
    @micropost.detail_mainboard = "a" * 256
    @micropost.ram = "a" * 256
    @micropost.detail_ram = "a" * 256
    @micropost.storage = "a" * 256
    @micropost.detail_storage = "a" * 256
    @micropost.vga = "a" * 256
    @micropost.detail_vga = "a" * 256
    @micropost.power = "a" * 256
    @micropost.detail_power = "a" * 256
    @micropost.case = "a" * 256
    @micropost.detail_case = "a" * 256
    assert_not @micropost.valid?
  end
=end
  
  test "cpu should be at most 255 characters" do
    @micropost.cpu = "a" * 256
    assert_not @micropost.valid?
  end
  
  test "detail_cpu should be at most 255 characters" do
    @micropost.detail_cpu = "a" * 256
    assert_not @micropost.valid?
  end
  
  test "mainboard should be at most 255 characters" do
    @micropost.mainboard = "a" * 256
    assert_not @micropost.valid?
  end
  
  test "detail_mainboard should be at most 255 characters" do
    @micropost.detail_mainboard = "a" * 256
    assert_not @micropost.valid?
  end
  
  test "ram should be at most 255 characters" do
    @micropost.ram = "a" * 256
    assert_not @micropost.valid?
  end
  
  test "detail_ram should be at most 255 characters" do
    @micropost.detail_ram = "a" * 256
    assert_not @micropost.valid?
  end
  
  test "storage should be at most 255 characters" do
    @micropost.storage = "a" * 256
    assert_not @micropost.valid?
  end
  
  test "detail_storage should be at most 255 characters" do
    @micropost.detail_storage = "a" * 256
    assert_not @micropost.valid?
  end
  
  test "vga should be at most 255 characters" do
    @micropost.vga = "a" * 256
    assert_not @micropost.valid?
  end
  
  test "detail_vga should be at most 255 characters" do
    @micropost.detail_vga = "a" * 256
    assert_not @micropost.valid?
  end
  
  test "power should be at most 255 characters" do
    @micropost.power = "a" * 256
    assert_not @micropost.valid?
  end
  
  test "detail_power should be at most 255 characters" do
    @micropost.detail_power = "a" * 256
    assert_not @micropost.valid?
  end
  
  test "case should be at most 255 characters" do
    @micropost.case = "a" * 256
    assert_not @micropost.valid?
  end
  
  test "detail_case should be at most 255 characters" do
    @micropost.detail_case = "a" * 256
    assert_not @micropost.valid?
  end
  
  test "order should be most recent first" do
    assert_equal microposts(:most_recent), Micropost.first
  end
   
  
  #### Update ####
  
  test "can change cpu" do
    @micropost.cpu = "Intel Core i3-6100"
    assert_equal @micropost.cpu,"Intel Core i3-6100"
  end

  test "can change detail_cpu" do
    @micropost.detail_cpu = "Advice, 4290"
    assert_equal @micropost.detail_cpu,"Advice, 4290"
  end

  test "can change mainboard" do
    @micropost.mainboard = "ASUS H110M-K"
    assert_equal @micropost.mainboard,"ASUS H110M-K"
  end
  
  test "can change detail_mainboard" do
    @micropost.detail_mainboard = "Advice, 1950"
    assert_equal @micropost.detail_mainboard,"Advice, 1950"
  end
 
  test "can change ram" do
    @micropost.ram = "Hynix DDR400 1GB"
    assert_equal @micropost.ram,"Hynix DDR400 1GB"
  end
  
  test "can change detail_ram" do
    @micropost.detail_ram = "Advice, 590"
    assert_equal @micropost.detail_ram,"Advice, 590"
  end

  test "can change storage" do
    @micropost.storage = "WD 4TB"
    assert_equal @micropost.storage,"WD 4TB"
  end
  
  test "can change detail_storage" do
    @micropost.detail_storage = "Lazada, 3790"
    assert_equal @micropost.detail_storage,"Lazada, 3790"
  end

  test "can change vga" do
    @micropost.vga = "ASUS DUAL-GTX1060-O6G"
    assert_equal @micropost.vga,"ASUS DUAL-GTX1060-O6G"
  end
  
  test "can change detail_vga" do
    @micropost.detail_vga = "JIB, 8990"
    assert_equal @micropost.detail_vga,"JIB, 8990"
  end

  test "can change power" do
    @micropost.power = "DELUX V6"
    assert_equal @micropost.power,"DELUX V6"
  end
  
  test "can change detail_power" do
    @micropost.detail_power = "JIB, 990"
    assert_equal @micropost.detail_power,"JIB, 990"
  end

  test "can change case" do
    @micropost.case = "DEEPCOOL DUKASE V3"
    assert_equal @micropost.case,"DEEPCOOL DUKASE V3"
  end
  
  test "can change detail_case" do
    @micropost.detail_case = "JIB, 1990"
    assert_equal @micropost.detail_case,"JIB, 1990"
  end

  #### Search ####
  
  test "not finds a searched project by price" do
    assert_no_difference '(Micropost.search(80000,100000)).count' do
      @user.microposts.create(cpu:"INTEL Corei7-8700K", detail_cpu:"JIB, 35000",mainboard: "asus z270",detail_mainboard: "jib 10000",ram:"Kington DDR100 1TB",detail_ram:"JIB, 2000",storage:"WD 4 TB",detail_storage:"JIB, 1000",vga:"ASUS Geforce 2080TI",detail_vga:"JIB, 3000",power:"CORSAIR 20000 watt",detail_power:"JIB, 3000",case:"Gigabyte modelz",detail_case:"JIB, 1500",price: 70000)
    end
  end
  
  test "finds a searched project by price should be found" do
    assert_difference '(Micropost.search(80000,100000)).count', +1 do
      @user.microposts.create(cpu:"INTEL Corei7-8700K", detail_cpu:"JIB, 35000",mainboard: "asus z270",detail_mainboard: "jib 10000",ram:"Kington DDR100 1TB",detail_ram:"JIB, 2000",storage:"WD 4 TB",detail_storage:"JIB, 1000",vga:"ASUS Geforce 2080TI",detail_vga:"JIB, 3000",power:"CORSAIR 20000 watt",detail_power:"JIB, 3000",case:"Gigabyte modelz",detail_case:"JIB, 1500",price: 87000)
    end
  end
  
  test "finds all should be show all object" do
    assert_equal Micropost.count,(Micropost.all).count
  end
  
#>>>>>>> ite3 
  
end