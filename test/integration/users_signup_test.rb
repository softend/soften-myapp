require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  
  
  test "name should be at most 50 characters" do
    get signup_path
    assert_no_difference 'User.count' do
      @name = "a" * 51
      post users_path, params: { user: { name:  @name,
                                         email: "user@invalid",
                                         password: "123456",
                                         password_confirmation: "123456" } }
    end
    assert_template "users/new"
  end
  
  test "email should be at most 250 characters" do
    get signup_path
    assert_no_difference 'User.count' do
      @username = "u" * 240
      @domaindotcom = "d" * 10
      post users_path, params: { user: { name:  "natchawi",
                                         email: @username+'@'+@domaindotcom,
                                         password: "123456",
                                         password_confirmation: "123456" } }
    end
    assert_template "users/new"
  end
  
  test "email should be use aroba(@)" do
    get signup_path
    assert_no_difference 'User.count' do
      post users_path, params: { user: { name:  "natchawi",
                                         email: "userinvalid.com",
                                         password: "123456",
                                         password_confirmation: "123456" } }
    end
    assert_template "users/new"
  end
  
  test "password should be at lease 6 characters  " do
    get signup_path
    assert_no_difference 'User.count' do
      @password = "p" * 3
      post users_path, params: { user: { name:  "natchawi",
                                         email: "user@invalid",
                                         password: @password,
                                         password_confirmation: @password } }
    end
    assert_template "users/new"
  end
  
  test "password comfirmation should be same password" do
    get signup_path
    assert_no_difference 'User.count' do
      post users_path, params: { user: { name:  "natchawi",
                                         email: "user@invalid",
                                         password: "123456",
                                         password_confirmation: "1234" } }
    end
    assert_template "users/new"
  end
  
  test "valid signup information" do
    get signup_path
    assert_difference 'User.count', 1 do
      post users_path, params: { user: { name:  "natchawi",
                                         email: "natchawi@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    end
    follow_redirect!
    assert_template 'users/show'
    assert is_logged_in?
  end
end
