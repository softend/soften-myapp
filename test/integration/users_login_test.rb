require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:natchawi)
    
    #from fixture
    #natchawi:
    #name: Natchawi
    #email: natchawi@tu.com
    #password_digest: <%= User.digest('password') %>
    
  end
  
  test "login with invalid email" do
    get signin_path
    @testUser = User.create( email: 'wrong-email', password: 'password')
    assert_not_equal @user.email, @testUser.email
  end
  
  test "login with invalid password" do
    get signin_path
    @testUser = User.create( email: @user.email, password: 'wrong-password')
    assert_not_equal @user.password, @testUser.password
  end

  
  test "login with valid information" do
    get signin_path
    post signin_path, params: { session: { email:    @user.email,
                                          password: 'password' } }
    assert_redirected_to @user
    follow_redirect!
    assert_template 'users/show'
    assert_select "a[href=?]", signin_path, count: 0 #ดูว่ามีปุ่มsigninไหม
    assert_select "a[href=?]", signout_path
    assert_select "a[href=?]", user_path(@user)
  end
  
  test "login with valid information followed by logout" do
    get signin_path
    post signin_path,params:{session:{email: @user.email,password:'password'}}
    assert is_logged_in?
    assert_redirected_to @user
    follow_redirect!
    assert_template 'users/show'
    assert_select "a[href=?]", signin_path,count: 0
    assert_select "a[href=?]", signout_path
    assert_select "a[href=?]", user_path(@user)
    delete signout_path
    assert_not is_logged_in?
    assert_redirected_to root_url
    follow_redirect!
    assert_select "a[href=?]", signin_path
    assert_select "a[href=?]", signout_path,      count: 0
    assert_select "a[href=?]", user_path(@user), count: 0
  end
end
